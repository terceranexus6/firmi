# Firmi

Firmwalker based web interface for easy firmware representation and carving.

![](./images/logo.png)


The main information is gathered from [firmwalker](https://github.com/scriptingxss/firmwalker), a great tool for firmware analysis, please take a look at it.

![](./images/example.jpg)

## Fast setup:

After downloading the repo, in the main directory, download firmwalker:

```
$ git clone https://github.com/scriptingxss/firmwalker.git
```

If you have a binary file:

copy the binary file inside de directory and:

```
$ ./firmi.sh myfile.bin
```

If you don't have a binary file, already the firmwalker.txt report:

Place the report under the name firmwalker.txt in the main directory and:

```
$ ./firmi.sh 
```

Troubleshooting:

* Remember to name the firmwalker file as firmwalker.txt if you are importing it.
* Use `chmod` in case of permissions troubles.
* Still working on it! accepting MR, take a look at the issues if you want to help


