#!/bin/bash

# FIRMI
# Firmwalker web interface for quick review
# GPLv3

#colors
RED='\033[0;31m'
PINK='\033[0;35m'
NC='\033[0m' # No Color

#user
MYUSER=$(whoami)

#creating a file for html launching afterwards
cp template.html final.html

sed -i "s/user/$MYUSER/" final.html


cat ./text/signature
cat ./text/help_g 


#$1 is bin or help

if [ -z "$1" ]
then

    if [ "$1" = "-h" ]; then
        cat ./text/help_g
    else
	    echo -e "${PINK}input file detected, preparing everything...${NC}"


	    #binwalk reporting
	    binwalk $1 > binwalk_report

	    #looking for signatures in the header
	    echo -e "${PINK}Signatures in header...${NC}"
	    hexdump -C $1 | head > header_sig
	    cat header_sig

	    #carving the squashfs filesystem
	    echo -e "${PINK}Carving the squashfs filesystem...${NC}"
	    dd if=$1 bs=1 skip=1704084 of=dir.squashfs
	    unsquashfs dir.squashfs

	    #executing firmwalker and letting it save a report in the same directory
	    ../firmwalker/firmwalker.sh _* firmwalker.txt
	fi

else #we already have the firmwalker file


    echo -e "${PINK}Counting SSL files..."${NC}

    #counting pem files
    grep -o '.pem' firmwalker.txt | wc -l > num1
    NUM=$(<num1)
    NUM=$((10#$NUM))
    NUM1=$((NUM-1))
    rm num1

    sed -i "s/PEM_F/$NUM1/" final.html

    #counting .key
    grep -o '.key' firmwalker.txt | wc -l > num2
    NUM=$(<num2)
    NUM=$((10#$NUM))
    NUM2=$((NUM-1))
    rm num2

    sed -i "s/KEY_F/$NUM2/" final.html

    #counting .cer
    grep -o '.cer' firmwalker.txt | wc -l > num3
    NUM=$(<num3)
    NUM=$((10#$NUM))
    NUM3=$((NUM-1))
    rm num3

    sed -i "s/CER_F/$NUM3/" final.html


    #counting .p7b
    grep -o '.p7b' firmwalker.txt | wc -l > num4
    NUM=$(<num4)
    NUM=$((10#$NUM))
    NUM4=$((NUM-1))
    rm num4

    sed -i "s/P7B_F/$NUM4/" final.html


    #counting .p12
    grep -o '.p12' firmwalker.txt | wc -l > num5
    NUM=$(<num5)
    NUM=$((10#$NUM))
    NUM5=$((NUM-1))
    rm num5

    sed -i "s/P12_F/$NUM5/" final.html

    #counting .crt
    grep -o '.crt' firmwalker.txt | wc -l > num6
    NUM=$(<num6)
    NUM=$((10#$NUM))
    NUM6=$((NUM-1))
    rm num6

    sed -i "s/CRT_F/$NUM6/" final.html

    SSL_total=$(($NUM1+$NUM2+$NUM3+$NUM4+$NUM5+$NUM6))
    sed -i "s/NUM1/$SSL_total/" final.html

    #counting passwd
    grep -o '/passwd' firmwalker.txt | wc -l > num7
    NUM=$(<num7)
    NUM7=$((10#$NUM))
    rm num7

    sed -i "s/NUM3/$NUM7/" final.html

    #counting IPs
    grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" firmwalker.txt | wc -l > num8
    NUM=$(<num8)
    NUM8=$((10#$NUM))
    rm num8

    sed -i "s/NUM2/$NUM8/" final.html

    #counting emails
    grep -i -o '[A-Z0-9._%+-]\+@[A-Z0-9.-]\+\.[A-Z]\{2,4\}' firmwalker.txt | wc -l > num9
    NUM=$(<num9)
    NUM9=$((10#$NUM))
    rm num9

    sed -i "s/NUM4/$NUM9/" final.html

    #Calculating risk level

    #SSL
    if [ $SSL_total -ge 1 ]; then
	    if [ $SSL_total -gt 6 ]; then
		    risk_l=3
	    else
		    risk_l=2
	    fi
    else
	    risk_l=1

    fi

    #passwd
    if [ $NUM7 -ge 1 ]; then
	    risk_l=$(($((10#$risk_l))+1))
    fi

    #IP
    if [ $NUM8 -gt 1 ]; then
	    risk_l=$(($((10#$risk_l))+1))
    fi

    #email
    if [ $NUM9 -gt 1 ]; then
	    risk_l=$(($((10#$risk_l))+1))
    fi


    #total

    #HMTL HELP
    LOW_PC="10%"
    LOW_COLOR="green"

    MEDIUM_PC="50%"
    MEDIUM_COLOR="yellow"

    HIGH_PC="80%"
    HIGH_COLOR="pink"


    LIMIT1=$((10#1))
    LIMIT2=$((10#4))
    LIMIT3=$((10#5))

    if [ $risk_l -eq $LIMIT1 ]; then
	    total_risk="LOW"
	    #substitution in file
	    sed -i "s/RISK_PC/$LOW_PC/" final.html
	    sed -i "s/RISK_COLOR/$LOW_COLOR/" final.html

    elif [ $risk_l -lt $LIMIT2 ]; then 
	    total_risk="MEDIUM"
	    #substitution in file
            sed -i "s/RISK_PC/$MEDIUM_PC/" final.html
            sed -i "s/RISK_COLOR/$MEDIUM_COLOR/" final.html

    elif [ $risk_l -ge $LIMIT3 ]; then
	    total_risk="HIGH"
	    #substitution in file
            sed -i "s/RISK_PC/$HIGH_PC/" final.html
            sed -i "s/RISK_COLOR/$HIGH_COLOR/" final.html
    else
	    total_risk="ERROR"
    fi

fi

echo -e "${PINK}Opening link...${NC}"

#opening link in background
firefox final.html &














